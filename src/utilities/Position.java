package utilities;

public class Position {
      
      private double x;
      private double y;

     Position(double x, double y)
     {
         this.x = x;
         this.y = y;
     }      

     public double getX() {
         return x;
     }

     public double getY() {
         return y;
     }

     public void setPosition(Position pos)
     {
         this.x = pos.x;
         this.y = pos.y;


     }

}
