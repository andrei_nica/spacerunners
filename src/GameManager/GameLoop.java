package GameManager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.awt.*;

import javax.swing.JFrame;

public class GameLoop extends JFrame  {

    private static final int FPS = 60;
    private int WIDTH = 1920 ;
    private int HEIGHT  = 1080;

    public static void run() {

        new GameLoop().startGame();
    }

    private void startGame() {

             this.setSize(WIDTH, HEIGHT);
             this.setVisible(true);;

      ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new Runnable() 
        {
            @Override
            public void run() {

                  
                updateGame();


            }
        }, 0, 1000 / FPS, TimeUnit.MILLISECONDS);
    }

    private void updateGame() {

        
        System.out.println("game updated");
      
        // the game code
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
       // super.paintComponent(g);
        g2.setColor(Color.red);
        g2.drawRect(10, 10, 100, 100);
    }

}
